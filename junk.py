test_cases = [
        {'id': 1, 'name': 'Корень', 'parent': None},
        {'id': 2, 'name': 'Ветвь 1', 'parent': 1},
        {'id': 3, 'name': 'Ветвь 2', 'parent': 1},
        {'id': 4, 'name': 'Подветвь 1.1', 'parent': 2},
        {'id': 5, 'name': 'Подветвь 1.2', 'parent': 2},
        {'id': 6, 'name': 'Подветвь 2.1', 'parent': 3},
        {'id': 7, 'name': 'Подветвь 2.2', 'parent': 3},
        {'id': 8, 'name': 'Корень 2', 'parent': None},
        {'id': 9, 'name': 'Ветвь 1', 'parent': 8},
        {'id': 10, 'name': 'Ветвь 2', 'parent': 8},
]


def generate_tree(id):
    return {
        'id': id,
        'name': [x['name'] for x in test_cases if x['id'] == id][0],
        'children': [generate_tree(x['id']) for x in test_cases
                     if x['parent'] == id]
    }


print(generate_tree(1))
print(generate_tree(8))
print(generate_tree(3))
