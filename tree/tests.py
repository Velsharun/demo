from django.test import TestCase, RequestFactory
from django.db import connection, reset_queries
from django.conf import settings

from tree.models import Tree
from tree.views import IndexView, AllView, ChildView


class TreeModelTests(TestCase):
    """
    Тесты для модели Tree
    """
    
    def setUp(self):
        t1 = Tree.objects.create(parent=None, name='Test')
        t2 = Tree.objects.create(parent=None, name='Test2')
        t3 = Tree.objects.create(parent=t1, name='Test branch 1')
        Tree.objects.create(parent=t1, name='Test branch 1.2')
        Tree.objects.create(parent=t2, name='Test branch 2')
        t4 = Tree.objects.create(parent=t2, name='Test branch 2.2')
        Tree.objects.create(parent=t3, name='Test branch 3')
        Tree.objects.create(parent=t4, name='Test branch 6')
        
    def test_generate_tree_empty_key(self):
        """
        Проверка метода на получение пустого ключа
        """

        self.assertEqual(Tree.generate_tree(Tree, None), 'Пустой ключ')
        
    def test_generate_tree_mising_key(self):
        """
        Проверка метода на получение отсутсвующего ключа
        """

        self.assertEqual(
            Tree.generate_tree(Tree, 15),
            'Ключ отстутсвует в базе'
        )
        
    def test_generate_tree_all(self):
        """
        Проверка получения всех корней деревьев
        """

        test_case = [
            {'id': 1, 'name': 'Test', 'children': [
                {'id': 3, 'name': 'Test branch 1', 'children': [
                    {'id': 7, 'name': 'Test branch 3', 'children': []}
                ]},
                {'id': 4, 'name': 'Test branch 1.2', 'children': []}
            ]},
            {'id': 2, 'name': 'Test2', 'children': [
                {'id': 5, 'name': 'Test branch 2', 'children': []},
                {'id': 6, 'name': 'Test branch 2.2', 'children': [
                    {'id': 8, 'name': 'Test branch 6', 'children': []}
                ]}]
             }
        ]

        self.assertEqual(
            test_case,
            [Tree.generate_tree(Tree, x.id)
             for x in Tree.objects.filter(parent=None)]
        )

    def test_generate_tree_key(self):
        """
        Проверка получения узла по ключу
        """

        test_case = [
            {'id': 3, 'name': 'Test branch 1', 'children': [
                {'id': 7, 'name': 'Test branch 3', 'children': []}
            ]},
            {'id': 6, 'name': 'Test branch 2.2', 'children': [
                {'id': 8, 'name': 'Test branch 6', 'children': []}
            ]}
        ]
        
        self.assertEqual(test_case[0], Tree.generate_tree(Tree, 3))
        self.assertEqual(test_case[1], Tree.generate_tree(Tree, 6))

    def test_generate_tree_optimized_all(self):
        """
        Проверка оптимизированной функции при получении всех корней деревьев
        """

        settings.DEBUG = True
        test_case = [
            {'id': 1, 'name': 'Test', 'children': [
                {'id': 3, 'name': 'Test branch 1', 'children': [
                    {'id': 7, 'name': 'Test branch 3', 'children': []}
                ]},
                {'id': 4, 'name': 'Test branch 1.2', 'children': []}
            ]},
            {'id': 2, 'name': 'Test2', 'children': [
                {'id': 5, 'name': 'Test branch 2', 'children': []},
                {'id': 6, 'name': 'Test branch 2.2', 'children': [
                    {'id': 8, 'name': 'Test branch 6', 'children': []}
                ]}]
             }
        ]

        reset_queries()
        self.assertEqual(
            test_case,
            [Tree.generate_tree(Tree, x.id)
             for x in Tree.objects.filter(parent=None)]
        )
        # число запросов к базе
        generate_old_count = len(connection.queries)
        # сброс счетчика
        reset_queries()
        self.assertEqual(
            test_case,
            [Tree.generate_tree_optimized(Tree, x.id)
             for x in Tree.objects.filter(parent=None)]
        )
        generate_lru_count = len(connection.queries)
        self.assertTrue(generate_old_count > generate_lru_count)

    def test_generate_tree_optimized_key(self):
        """
        Проверка оптимизированной функции при получении узла по ключу
        """

        settings.DEBUG = True
        test_case = [
            {'id': 3, 'name': 'Test branch 1', 'children': [
                {'id': 7, 'name': 'Test branch 3', 'children': []}
            ]},
            {'id': 6, 'name': 'Test branch 2.2', 'children': [
                {'id': 8, 'name': 'Test branch 6', 'children': []}
            ]}
        ]

        reset_queries()
        self.assertEqual(test_case[0], Tree.generate_tree(Tree, 3))
        self.assertEqual(test_case[1], Tree.generate_tree(Tree, 6))
        # число запросов к базе
        generate_old_count = len(connection.queries)
        # сброс счетчика
        reset_queries()
        self.assertEqual(test_case[0], Tree.generate_tree_optimized(Tree, 3))
        self.assertEqual(test_case[1], Tree.generate_tree_optimized(Tree, 6))
        generate_lru_count = len(connection.queries)
        self.assertTrue(generate_old_count >= generate_lru_count)


class TestIndexView(TestCase):
    """
    Тесты для IndexView
    """
    
    def test_index(self):
        """
        Проверка доступности страницы
        """
        
        request = RequestFactory().get('/tree/')
        view = IndexView()
        view.setup(request)

        context = view.get(request)
        self.assertEqual(context.status_code, 200)


class TestAllView(TestCase):
    """
    Тесты для AllView
    """
    
    def test_all(self):
        """
        Проверка доступности страницы
        """
        
        request = RequestFactory().get('/tree/all/')
        view = AllView()
        view.setup(request)

        context = view.get(request)
        self.assertEqual(context.status_code, 200)
    
    def test_get_queryset(self):
        """
        Проверка метода get_queryset
        """

        request = RequestFactory().get('/tree/all/')
        view = AllView()
        view.setup(request)

        query = view.get_queryset()
        self.assertIsNotNone(query)


class TestChildView(TestCase):
    """
    Тесты для ChildView
    """

    def test_get_queryset(self):
        """
        Проверка метода get_queryset
        """

        request = RequestFactory().get('/tree/childs/')
        view = ChildView()
        view.setup(request)

        query = view.get_queryset()
        self.assertIsNotNone(query)
        
    def test_child(self):
        """
        Проверка доступности страницы при отсутствии ключа в базе
        """
    
        request = RequestFactory().post('/tree/childs/', {'child': 1})
        view = ChildView()
        view.setup(request)
    
        context = view.post(request)
        self.assertEqual(context.status_code, 200)
        self.assertEqual(
            context.content,
            b'\xd0\x9a\xd0\xbb\xd1\x8e\xd1\x87 \xd0\xbe\xd1\x82\xd1\x81\xd1\x82\xd1\x83\xd1\x82\xd1\x81\xd0\xb2\xd1\x83\xd0\xb5\xd1\x82 \xd0\xb2 \xd0\xb1\xd0\xb0\xd0\xb7\xd0\xb5'
        )

    def test_child_empty(self):
        """
        Проверка доступности страницы при пустом POST запросе
        """
    
        request = RequestFactory().post('/tree/childs/', {'child': ''})
        view = ChildView()
        view.setup(request)
        context = view.post(request)
        self.assertEqual(context.status_code, 200)
        self.assertEqual(
            context.content,
            b'\xd0\x9f\xd1\x83\xd1\x81\xd1\x82\xd0\xbe\xd0\xb9 \xd0\xba\xd0\xbb\xd1\x8e\xd1\x87'
        )
        
    def test_child_has_key(self):
        """
        Проверка доступности страницы при указании существующего ключа
        """

        t = Tree.objects.create(parent=None, name='Test')
        request = RequestFactory().post('/tree/childs/', {'child': 1})
        view = ChildView()
        view.setup(request)

        context = view.post(request)
        self.assertEqual(context.status_code, 200)
        self.assertEqual(
            context.content,
            b"{'id': '1', 'name': 'Test', 'children': []}"
        )
