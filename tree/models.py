from django.db import models


class Tree(models.Model):
    parent = models.ForeignKey('self', models.CASCADE, null=True,
                               verbose_name='Родитель')
    name = models.CharField(max_length=255, verbose_name='Название')

    def generate_tree(self, key, name=None):
        """Генерация дерева по id"""
        
        if key:
            try:
                return {
                    'id': key,
                    'name': name if name else Tree.objects.get(pk=key).name,
                    'children': [self.generate_tree(self, x.id, x.name) for x
                                 in Tree.objects.filter(parent=key)]
                }
            except models.ObjectDoesNotExist:
                return 'Ключ отстутсвует в базе'
        else:
            return 'Пустой ключ'

    def generate_tree_optimized(self, key):
        """Генерация дерева по id (меньшее кол-во запросов к базе)"""

        def flat_list(data):
            """Развертывание вложенных списков"""
            return [
                item for sublist in data for item in sublist
            ]

        def create_record(tree_list):
            """Преобразование списка объектов Tree в список словарей"""
            result = []

            def create(node):
                """Преобразование объекта Tree в словарь"""
                return {
                    'id': node.id,
                    'name': node.name,
                    'children': [create(x) for x in tree_list if
                                 x.parent_id
                                 == node.id]
                }
            for tree in tree_list:
                result.append(create(tree))
            return result[0]

        def generate(node, data=[]):
            if node:
                if isinstance(node, int):
                    qs = Tree.objects.filter(id=node)
                    ds = list(qs)
                    data.append(ds)
                    return generate(qs, data)
                else:
                    qs = Tree.objects.filter(parent__in=node)
                    if qs:
                        ds = list(qs)
                        data.append(ds)
                        return generate(qs, data)
                    else:
                        return create_record(flat_list(data))
        return generate(key)
