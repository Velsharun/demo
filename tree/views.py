from django.shortcuts import render
from django.views import generic
from django.http import HttpResponse, Http404

from .models import Tree


class IndexView(generic.View):
    template_name = 'tree/index.html'
    
    def get(self, request):
        return render(request, 'tree/index.html')


class AllView(generic.ListView):
    """
    Отображение всех данных из модели
    Возможно использование JSON вместо шаблона
    """

    model = Tree
    template_name = 'tree/all.html'
    context_object_name = 'tree_list'

    def get_queryset(self):
        return (Tree.generate_tree(Tree, x.id) for x in Tree.objects.filter(parent=None))

    
class ChildView(generic.View):
    """
    Отображение потомков по выбранному элементу
    """
    model = Tree
    template_name = 'tree/childs.html'
    context_object_name = 'child'
    node = None
    
    def get_queryset(self):
        return Tree.generate_tree(Tree, self.node)
    
    def post(self, request):
        self.node = request.POST['child']
        return HttpResponse(str(Tree.generate_tree(Tree, self.node)))
