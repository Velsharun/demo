from django.urls import path

from . import views

app_name = 'tree'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('all/', views.AllView.as_view(), name='all'),
    path('childs/', views.ChildView.as_view(), name='childs'),
]
